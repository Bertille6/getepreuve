<?php

namespace ContainerMbONEj4;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder2fafa = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5ac45 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesaec64 = [
        
    ];

    public function getConnection()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getConnection', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getMetadataFactory', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getExpressionBuilder', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'beginTransaction', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getCache', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getCache();
    }

    public function transactional($func)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'transactional', array('func' => $func), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->transactional($func);
    }

    public function commit()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'commit', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->commit();
    }

    public function rollback()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'rollback', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getClassMetadata', array('className' => $className), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'createQuery', array('dql' => $dql), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'createNamedQuery', array('name' => $name), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'createQueryBuilder', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'flush', array('entity' => $entity), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'clear', array('entityName' => $entityName), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->clear($entityName);
    }

    public function close()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'close', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->close();
    }

    public function persist($entity)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'persist', array('entity' => $entity), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'remove', array('entity' => $entity), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'refresh', array('entity' => $entity), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'detach', array('entity' => $entity), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'merge', array('entity' => $entity), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getRepository', array('entityName' => $entityName), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'contains', array('entity' => $entity), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getEventManager', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getConfiguration', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'isOpen', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getUnitOfWork', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getProxyFactory', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'initializeObject', array('obj' => $obj), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'getFilters', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'isFiltersStateClean', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'hasFilters', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return $this->valueHolder2fafa->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer5ac45 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder2fafa) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder2fafa = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder2fafa->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, '__get', ['name' => $name], $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        if (isset(self::$publicPropertiesaec64[$name])) {
            return $this->valueHolder2fafa->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2fafa;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2fafa;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2fafa;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder2fafa;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, '__isset', array('name' => $name), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2fafa;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder2fafa;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, '__unset', array('name' => $name), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder2fafa;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder2fafa;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, '__clone', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        $this->valueHolder2fafa = clone $this->valueHolder2fafa;
    }

    public function __sleep()
    {
        $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, '__sleep', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;

        return array('valueHolder2fafa');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer5ac45 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer5ac45;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer5ac45 && ($this->initializer5ac45->__invoke($valueHolder2fafa, $this, 'initializeProxy', array(), $this->initializer5ac45) || 1) && $this->valueHolder2fafa = $valueHolder2fafa;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder2fafa;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder2fafa;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
